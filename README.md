# react-multi-thumb-slider

This package provides you general non-overlapping multi thumb range slider.

> Warning: beta quality software!
> use at your own risk

### Installing

```shell script
npm install --save react-multi-thumb-slider
```

or

```shell script
yarn add react-multi-thumb-slider
```


### Importing

#### General slider
```javascript
import { Slider } from 'react-multi-thumb-slider';
```

#### Material slider preset
```javascript
import { MaterialSlider } from 'react-multi-thumb-slider';
```

### Gallery

#### Material slider preset

![Material slider preset](./assets/materialSlider.png)

```jsx
<div style={{ width: 500 }}>
    <MaterialSlider min={0} max={10} values={values} onChange={setValues} accentColor="#cc5803" />

    <p>
        {values[0]}
    </p>

    <MaterialSlider min={0} max={10} values={values1} onChange={setValues1} accentColor="#005803" />

    <p>
        {values1.join(',')};
    </p>

    <MaterialSlider min={1} max={10} values={values2} onChange={setValues2} accentColor="#cc0003" />

    <p>
        {values2.join(',')}
    </p>
</div>
```

#### Custom slider

![Custom slider](./assets/customSlider.png)


```jsx
<Slider
    min={1}
    max={10}
    onChange={setValues}
    values={values}
    styleThumb={{background: '#333', height: '20px', width: '20px'}}
    styleTrack={{background: 'lightblue', borderRadius: '2px', height: '10px', marginTop: '5px'}}
    styleWrap={{height: '20px', width: '300px'}}
    rangeBackground="grey"
/>
```

### Pay attention

- values aren't sorted


### Todo
- implement thumb labels
- docs
- tests
- more presets


### Used resources

- https://css-tricks.com/multi-thumb-sliders-general-case.
