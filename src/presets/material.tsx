import * as React from 'react';
import Slider from "../slider";
import {FC} from "react";


export interface MaterialSliderProps {
    min: number;
    max: number;
    values: number[];
    onChange: (values: number[]) => any;

    accentColor: string;

    width?: string;
    height?: string;
    trackHeight?: string;
}


const MaterialSlider: FC<MaterialSliderProps> = ({min, max, values, onChange, accentColor, height, width, trackHeight }) => {
    height = height || '15px';
    trackHeight = trackHeight || '5px';
    width = width || '100%';

    return (
        <Slider
            min={min}
            max={max}
            values={values}
            onChange={onChange}
            styleWrap={{width, height}}
            styleThumb={{background: accentColor, height, width: height, borderRadius: '50%'}}
            styleTrack={{
                background: `${accentColor}61`,
                height: trackHeight,
                marginTop: `calc((${height} - ${trackHeight}) / 2)`,
                borderRadius: height,
            }}
            rangeBackground={accentColor}
        />
    );
};

export default MaterialSlider;