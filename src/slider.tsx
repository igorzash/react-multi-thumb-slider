import * as React from 'react';
import {CSSProperties, FC} from "react";

import './style.scss';
import styled, {css} from "styled-components";


export interface ThumbProps {
    border?: string;
    borderRadius?: string;
    background: string;
    height: string;
    width: string;
}

export interface TrackProps {
    border?: string;
    borderRadius?: string;
    background: string;
    height: string;
    marginTop?: string;
}

export interface WrapProps {
    height: string;
    width: string;
}


const cssThumbProps = (props: ThumbProps) => css`
  background: ${props.background};
  border: ${props.border};
  border-radius: ${props.borderRadius};
  width: ${props.width};
  height: ${props.height};
`;


const StyledInput = styled.input<{ thumb: ThumbProps }>`
	&::-webkit-slider-thumb {
	  ${props => cssThumbProps(props.thumb)};
	}
	
	&::-moz-range-thumb {
	  ${props => cssThumbProps(props.thumb)};
	}
`;

const Wrap = styled.div<{ rangeBackground: string, track: TrackProps, wrap: WrapProps }>`
  grid-template-rows: max-content ${props => props.wrap.height} max-content;
  
  width: ${props => props.wrap.width};

  &::before, &::after {
    border: ${props => props.track.border};
    border-radius: ${props => props.track.borderRadius};
    height: ${props => props.track.height};
    margin-top: ${props => props.track.marginTop};
  }
  
  &::before {
    background: ${props => props.track.background};
  }

  &::after {
    background: ${props => props.rangeBackground};
  }
`;

export interface SliderProps {
    min: number;
    max: number;
    step?: number;
    values: number[];
    onChange: (values: number[]) => any;

    styleWrap: WrapProps;
    styleThumb: ThumbProps;
    styleTrack: TrackProps;
    rangeBackground: string;
}

const Slider: FC<SliderProps> = (
    {
        min, max, step, values, onChange,
        styleWrap, styleThumb, styleTrack, rangeBackground
    }) => {
    const d = styleThumb.width;
    const uw = `calc(${styleWrap.width} - ${d})`;

    const tpos = values.map(
        (value, i) => `calc(var(--r) + (var(--v${i}) - var(--min))/var(--dif)*${uw})`
    );

    const fill = tpos.map(value => `linear-gradient(90deg, red ${value}, transparent 0)`);
    const hole = tpos.map(value => `radial-gradient(circle at ${value}, red var(--r), transparent 0)`);

    return (
        <Wrap
            rangeBackground={rangeBackground}
            wrap={styleWrap}
            track={styleTrack}
            className="wrap"
            style={{
                ...Object.fromEntries(values.map((value, i) => [`--v${i}`, value])),
                '--min': min,
                '--max': max,
                '--fill': fill.join(', '),
                '--hole': hole.join(', '),
            } as CSSProperties}>
            {values.map((value, i) => (
                <StyledInput
                    className="multi-thumb-slider"
                    type="range"
                    min={min}
                    max={max}
                    value={value}
                    step={step}
                    thumb={styleThumb}
                    onChange={(event) => {
                        const value = event.target.value;

                        const newValues = [...values];
                        newValues[i] = Number(value);

                        onChange(newValues);
                    }}
                />
            ))}
        </Wrap>
    );
};

export default Slider;
