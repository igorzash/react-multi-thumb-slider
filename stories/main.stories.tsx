import * as React from 'react';
import Slider from "../src/slider";
import {useState} from "react";
import MaterialSlider from "../src/presets/material";

export default ({
    title: 'Slider',
});


export const custom = () => {
    const [values, setValues] = useState([1, 3, 6, 5]);

    return (
        <Slider
            min={1}
            max={10}
            onChange={setValues}
            values={values}
            styleThumb={{background: '#333', height: '20px', width: '20px'}}
            styleTrack={{background: 'lightblue', borderRadius: '2px', height: '10px', marginTop: '5px'}}
            styleWrap={{height: '20px', width: '300px'}}
            rangeBackground="grey"
        />
    );
};

export const material = () => {
    const [values, setValues] = useState([5]);
    const [values1, setValues1] = useState([4, 6]);
    const [values2, setValues2] = useState([1, 3, 6, 5]);

    return (
        <div style={{ width: 500 }}>
            <MaterialSlider min={0} max={10} values={values} onChange={setValues} accentColor="#cc5803" />

            <p>
                {values[0]}
            </p>

            <MaterialSlider min={0} max={10} values={values1} onChange={setValues1} accentColor="#005803" />

            <p>
                {values1.join(',')}
            </p>

            <MaterialSlider min={1} max={10} values={values2} onChange={setValues2} accentColor="#cc0003" />

            <p>
                {values2.join(',')}
            </p>
        </div>
    );
}